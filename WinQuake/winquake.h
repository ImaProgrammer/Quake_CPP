/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// winquake.h: Win32-specific Quake header file

#pragma warning( disable : 4229 )  // mgraph gets this

#include <windows.h>
#define WM_MOUSEWHEEL                   0x020A

#ifndef SERVERONLY
#include <ddraw.h>
#include <dsound.h>
#ifndef GLQUAKE
#include <mgraph.h>
#endif
#endif

extern	HINSTANCE	global_hInstance;
extern	int			global_nCmdShow;

#ifndef SERVERONLY

extern LPDIRECTDRAW		lpDD;
extern bool			DDActive;
extern LPDIRECTDRAWSURFACE	lpPrimary;
extern LPDIRECTDRAWSURFACE	lpFrontBuffer;
extern LPDIRECTDRAWSURFACE	lpBackBuffer;
extern LPDIRECTDRAWPALETTE	lpDDPal;
extern LPDIRECTSOUND pDS;
extern LPDIRECTSOUNDBUFFER pDSBuf;

extern DWORD gSndBufSize;
//#define SNDBUFSIZE 65536

void	VID_LockBuffer (void);
void	VID_UnlockBuffer (void);

#endif

typedef enum {MS_WINDOWED, MS_FULLSCREEN, MS_FULLDIB, MS_UNINIT} modestate_t;

extern modestate_t	modestate;

extern HWND			mainwindow;
extern bool		ActiveApp, Minimized;

extern bool	WinNT;

int VID_ForceUnlockedAndReturnState (void);
void VID_ForceLockState (int lk);

void IN_ShowMouse (void);
void IN_DeactivateMouse (void);
void IN_HideMouse (void);
void IN_ActivateMouse (void);
void IN_RestoreOriginalMouseState (void);
void IN_SetQuakeMouseState (void);
void IN_MouseEvent (int mstate);

extern bool	winsock_lib_initialized;

extern cvar_t		_windowed_mouse;

extern int		window_center_x, window_center_y;
extern RECT		window_rect;

extern bool	mouseinitialized;
extern HWND		hwnd_dialog;

extern HANDLE	hinput, houtput;

void IN_UpdateClipCursor (void);
void CenterWindow(HWND hWndCenter, int width, int height, BOOL lefttopjustify);

void S_BlockSound (void);
void S_UnblockSound (void);

void VID_SetDefaultMode (void);

extern int (__stdcall *__stdcall pWSAStartup)(WORD wVersionRequired, LPWSADATA lpWSAData);
extern int (__stdcall *__stdcall pWSACleanup)(void);
extern int (__stdcall *__stdcall pWSAGetLastError)(void);
extern SOCKET (__stdcall *__stdcall psocket)(int af, int type, int protocol);
extern int (__stdcall *__stdcall pioctlsocket)(SOCKET s, long cmd, u_long *argp);
extern int (__stdcall *__stdcall psetsockopt)(SOCKET s, int level, int optname, const char * optval, int optlen);
extern int (__stdcall *__stdcall precvfrom)(SOCKET s, char * buf, int len, int flags,
extern struct sockaddr *from, int * fromlen);
extern int (__stdcall *__stdcall psendto)(SOCKET s, const char * buf, int len, int flags, const struct sockaddr *to, int tolen);
extern int (__stdcall *__stdcall pclosesocket)(SOCKET s);
extern int (__stdcall *__stdcall pgethostname)(char * name, int namelen);
extern struct hostent (__stdcall *__stdcall pgethostbyname)(const char * name);
extern struct hostent (__stdcall *__stdcall pgethostbyaddr)(const char * addr, int len, int type);
extern int (__stdcall *__stdcall pgetsockname)(SOCKET s, struct sockaddr *name, int * namelen);
